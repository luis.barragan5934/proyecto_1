<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articulos extends CI_Controller {
	public function __construct(){
		parent::__construct();
		// llamamos al modelo estudiante singular
			$this->load->model("articulo");
	}
// renderisamos la vista index estudiantes, presenta en el navegador la vist
	public function index()
	{
		$data["listadoArticulos"]=$this->articulo->obtenerTodos();
		$this->load->view('header');
		$this->load->view('articulos/index',$data);
		$this->load->view('footer');
	}
  // creamos nueva funcion con el NUEVO.php en vistas
	public function nuevo(){
		$this->load->view('header');
		$this->load->view('articulos/nuevo');
		$this->load->view('footer');
	}
                    

//funcion para capturar valores
	public function guardarEstudiantes(){
		$datosNuevoArticulos=array(
			"nombre_est"=>$this->input->post('nombre_est'),
			"fecha_nacimiento_est"=>$this->input->post('fecha_nacimiento_est')
		);
		print_r($datosNuevoArticulos);
		if ($this->articulo->insertar($datosNuevoArticulos)) {
			// echo "OK";
			redirect('articulos/index');
		}else{
			echo "EROOR NO HAY ARTICULOS";
			}
		}
///FUNCION para eliminar el estudiante echo en el modelo
	public function borrar($id_art){
		if ($this->articulo->eliminarPorId($id_art)) {
			// code...
			redirect('articulos/index');
		} else {
			// code...
			echo "ERROR AL ELIMINAR :()";
		}
	}

	public function actualizar($id){
		$data["articulosEditar"]=$this->articulo->obtenerPorId($id);
    	$this->load->view('header');
		$this->load->view('articulos/actualizar',$data);
		$this->load->view('footer');
  }

	//creamos la funcion para actualizr los estudiantes
	public function procesarActualizar(){
		$datosArticulosEditados=array(
			
			"nombre_est"=>$this->input->post('nombre_est'),			
			"fecha_nacimiento_est"=>$this->input->post('fecha_nacimiento_est')
		);
		// print_r($datosEstudiantesEditados);
		// el id que isimos hidden lo ponemos para
		$id=$this->input->post("id_est");
		if ($this->articulo->actualizar($id,$datosArticulosEditados)) {
			// echo "OK";
			redirect('articulos/index');
		}else{
			echo "EROOR";
			}
		}
}
