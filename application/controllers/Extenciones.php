<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Extenciones extends CI_Controller {
	public function index()
	{
		$this->load->view('header');
		$this->load->view('extenciones/index');
		$this->load->view('footer');
	}
  public function nuevo(){
    $this->load->view('header');
		$this->load->view('extenciones/nuevo');
		$this->load->view('footer');
  }
}
