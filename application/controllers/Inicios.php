<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicios extends CI_Controller {
	public function mision()
	{
		$this->load->view('header');
		$this->load->view('inicios/mision');
		$this->load->view('footer');
	}
  public function vision(){
    $this->load->view('header');
		$this->load->view('inicios/vision');
		$this->load->view('footer');
  }
}
