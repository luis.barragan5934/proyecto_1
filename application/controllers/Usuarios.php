<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
	public function index()
	{
		$this->load->view('header');
		$this->load->view('usuarios/index');
		$this->load->view('footer');
	}
  public function nuevo(){
    $this->load->view('header');
		$this->load->view('usuarios/nuevo');
		$this->load->view('footer');
  }
}
