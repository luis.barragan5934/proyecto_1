<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendedores extends CI_Controller {
	public function index()
	{
		$this->load->view('header');
		$this->load->view('vendedores/index');
		$this->load->view('footer');
	}
  public function nuevo(){
    $this->load->view('header');
		$this->load->view('vendedores/nuevo');
		$this->load->view('footer');
  }
}
