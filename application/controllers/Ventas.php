<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventas extends CI_Controller {
	public function index()
	{
		$this->load->view('header');
		$this->load->view('ventas/index');
		$this->load->view('footer');
	}
  public function nuevo(){
    $this->load->view('header');
		$this->load->view('ventas/nuevo');
		$this->load->view('footer');
  }
}
