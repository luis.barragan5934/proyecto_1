<?php
/**
 * Estudiante, ULTIMO PROCEDMIENTO
 * en el modelo siempre se elimina el php de cerrado
 */
class Articulo extends CI_Model
{
  function __construct()
  {
    // code...
  		parent::__construct();
  }
  public function insertar($datos){
    //ponemos el nombre de la tabla de la BDD CREADO
    return $this->db->insert("articulo",$datos);
  }
  // consulta de todos los estudiantes d ela base de datos
  public function obtenerTodos(){
    // linea de consulta
    $articulos=$this->db->get('articulo');
    if ($articulos->num_rows()>0) {
      return $articulos;
    } else {
      return false; //cuando no haya datos
    }
  }
  // funcion para eliminar estudiantes se necesito el id
  public function eliminarPorId($id){
    $this->db->where("id_art",$id);
    return $this->db->delete('articulo');
  }//
// nuevo
  // consultando datos por id para actualizar la knformacion
  public function obtenerPorId($id){
    $this->db->where("id_art",$id);
    $articulos=$this->db->get("articulo");
    if ($articulos->num_rows()>0) {
      return $articulos->row();//retorna datos
    } else {
      return false;
    }
  }
  //procso para actualizar
  public function actualizar($id,$datos){
    $this->db->where("id_est",$id);
    return $this->db->update("articulo",$datos);
  }

}//cierre de la clase no borrar
