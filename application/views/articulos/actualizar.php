<div class="row">
  <div class="col-md-12 text-center well">
    <h3>ACTUALIZAR </h3>
    <center>
      <a href="<?php echo site_url('estudiantes/index') ?>" class="btn btn-success">
      <i class="glyphicon glyphicon-repeat"></i>
      REGRESAR
      </a>
    </center>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <?php if ($estudiantesEditar): ?>
      <!-- <?php print_r($estudiantesEditar); ?> -->
      <!-- copiamos el formulario para haorrarnos tiempo -->
      <form  class="" action="<?php echo site_url('Articulos/procesarActualizar') ?>" method="post">
        <center>
          <input type="hidden" name="id_est" value="<?php echo $estudiantesEditar->id_est; ?> "></input>
          <!-- //hidden sirve para ocultar  el formulario -->
        </center>
        
        
        <br>
        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">Nombre</label>
          </div>
          <div class="col-md-7">
            <!-- el tipo de comentarios -->
            <input type="text" name="nombre_est" value="<?php echo $estudiantesEditar->nombre_est; ?>"
            class="form-control" placeholder="Ingrese sus Nombres" required>
          </div>
        </div>
  
      <br>
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">Fecha</label>
        </div>
        <div class="col-md-7">
          <input type="date" name="fecha_nacimiento_est" value="<?php echo $estudiantesEditar->fecha_nacimiento_est ?>"
          class="form-control" placeholder="Ingrese su fecha de Nacimiento" required>
        </div>
      </div>
      <br>
      <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-7">
          <!-- cambianos el nombre de button a submit -->
          <button type="submit" name="button"
                class="btn btn-warning">
              <i class="glyphicon glyphicon-ok"></i>
              ACTUALIZAR
          </button>
          <a href="<?php echo site_url('articulos/index') ?>" class="btn btn-danger">
            <i class="glyphicon glyphicon-remove"></i>
            CANCELAR
          </a>
        </div>
      </div>
      </form>

    <?php else: ?>
      <div class="alert alert-danger">
        <b>NO SE ENCONTRO</b>
      </div>
    <?php endif; ?>
  </div>
</div>
