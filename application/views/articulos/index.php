<legend class="text-center">
  <i class="glyphicon glyphicon-globe"></i>
    GESTION DE ARTICULOS
    <center>
      <a href="<?php echo site_url('articulos/nuevo') ?>" class="btn btn-success">
      <i class="glyphicon glyphicon-plus"></i>
      Agregar Nuevo
      </a>
    </center>
</legend>
<br>
<br>
<br>
<?php if ($listadoEstudiantes): ?>
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>  
      <th class="text-center">ID</th>       
        <th class="text-center">NOMBRE</th>        
        <th class="text-center">FECHA </th>
        <th class="text-center">ACCIONES</th>
      </tr>
    </thead>

    <tbody>
      <?php if ('listadoEstudiantes'): ?>

      <?php foreach ($listadoEstudiantes->result() as $estudianteTemporal): ?>
        <tr>   
        <td class="text-center"><?php echo $estudianteTemporal->id_est;?></td>       
        <td class="text-center"><?php echo $estudianteTemporal->nombre_est;?></td>
        <td class="text-center"><?php echo $estudianteTemporal->fecha_nacimiento_est; ?></td>
        <td class="text-center">
          <a href="<?php echo site_url('articulos/actualizar');?>/<?php echo $estudianteTemporal->id_est;?>" class="btn btn-warning">
            <i class="glyphicon glyphicon-edit"></i>
              Editar
          </a>
          <a href="<?php echo site_url('articulos/borrar');?>/<?php echo $estudianteTemporal->id_est;?>" class="btn btn-danger" onclick="return confirm('ESTAS PENDEJO ELIMINAR'); ">
            <i class="glyphicon glyphicon-trash"></i>
              Eliminar
          </a>
        </td>
      </tr>
      <?php endforeach; ?>
      <?php endif; ?>
    </tbody>

  </table>
<?php else: ?>
  <h3><b>NO EXISTEN ARTICULOS</b></h3>
  <!-- letra b bols de negrita -->
<?php endif; ?>

