<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAXV6fJ3AmkKcvYoxYWcAyQ5Y1ddJwSD68&libraries=places&callback=initMap" >
    </script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>credito_facil</title>
  </head>
  <body>
    <div class="row text-center">
      <div class="col-md-12">
        <img src="<?php echo base_url()?>/assets/images/BANNER.jpg" height="200px" width="100%" alt="logo">
      </div>
    </div>
    <nav class="navbar navbar-bg-ligh">
        <div class="container-fluid" style="background-color:#71c1a4;" >
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="col-md-1">
        </div>
        <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        <a class="navbar-brand" href="<?php echo site_url();?>">APP CREDITO FACIL</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">INICIO <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="<?php echo site_url('inicios/mision') ?>">Mision</a></li>
                <li><a href="<?php echo site_url('inicios/vision') ?>">Vision</a></li>
            </ul>
            </li>
            </ul>

            <ul class="nav navbar-nav">
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ARTICULOS<span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="<?php echo site_url('articulos/index') ?>">Listado</a></li>
                <li><a href="<?php echo site_url('articulos/nuevo') ?>">Nuevo</a></li>
            </ul>
            </li>
            </ul>

            <ul class="nav navbar-nav">
            <li class="dropdown">
            <a href="<?php echo site_url('asignatura/index') ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">VENTAS<span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="<?php echo site_url('ventas/index') ?>">Listado</a></li>
                <li><a href="<?php echo site_url('ventas/nuevo') ?>">Nuevo</a></li>
            </ul>
            </li>
            </ul>

            <ul class="nav navbar-nav">
            <li class="dropdown">
            <a href="<?php echo site_url('paralelos/index') ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">VENDEDORES<span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="<?php echo site_url('vendedores/index') ?>">Listado</a></li>
                <li><a href="<?php echo site_url('vendedores/nuevo') ?>">Nuevo</a></li>
            </ul>
            </li>
            </ul>

            <ul class="nav navbar-nav">
            <li class="dropdown">
            <a href="<?php echo site_url('asignatura/index') ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">EXTENSION<span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="<?php echo site_url('extenciones/index') ?>">Listado</a></li>
                <li><a href="<?php echo site_url('extenciones/nuevo') ?>">Nuevo</a></li>
            </ul>
            </li>
            </ul>

            <ul class="nav navbar-nav">
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">GALELRIA<span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="<?php echo site_url('galeria/index') ?>">Inicio</a></li>

            </ul>
            </li>
            </ul>

            <ul class="nav navbar-nav">
            <li class="dropdown">
            <a href="<?php echo site_url('asignatura/index') ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">USUARIOS<span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="<?php echo site_url('usuarios/index') ?>">Listado</a></li>
                <li><a href="<?php echo site_url('usuarios/nuevo') ?>">Nuevo</a></li>
            </ul>
            </li>
            </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
